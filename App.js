import React from 'react'
import { StyleSheet, Platform, Image, Text, View ,SafeAreaView, ScrollView,Dimensions,Animated,Easing,Button,TouchableOpacity} from 'react-native'
import {StackNavigator, DrawerItems,DrawerNavigator,createDrawerNavigator} from 'react-navigation'
import {Avatar} from 'react-native-elements'
import firebase from 'react-native-firebase';
import {Icon} from 'native-base'

// import the different screens

import Loading from './src/Loading'
import SignUp from './src/SignUp'
import Login from './src/Login'
import Main from './src/Main'
import ResetPassword from './src/ResetPassword'
import UserInfo from './src/UserInfo'
import ForgetPassword from './src/ForgetPassword'
import Question1 from './src/Question1'
import Question2 from './src/Question2'
import Question3 from './src/Question3'
import Question4 from './src/Question4'
import Question5 from './src/Question5'
import History from './src/History'
import Favorites from './src/Favorites'
import Logout from './src/Logout'
import Result from './src/Result'

// create our app's navigation stack
const {width} = Dimensions.get('window');

export default class App extends React.Component{
 
    render(){
        return(
          <PrimaryNav />
        )
    }
}



const CustomDrawerComponent = (props) =>(
  <SafeAreaView style = {{flex : 1}}>
    <View style = {{height: 200, alignItems:'center', backgroundColor:'#4583B8'}}>
        <Avatar
              containerStyle={{marginTop:50}}
              size ="large"
               source={{
               uri:
              'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
              }}
            rounded
            />  
    </View>
    <ScrollView>
      <DrawerItems {...props}/>
    </ScrollView>
  </SafeAreaView>
)

const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }
})

const DrawerStack = createDrawerNavigator({
  Home : Main,
  Search: Question1,
  History: History,
  Favorites: Favorites,
  Profile: UserInfo,
  Password: ResetPassword,
  Logout:Logout
},
  {  
    contentComponent: CustomDrawerComponent,
    draweWidth: width * 0.5,
    contentOptions:{
        activeTintColor:"#4583B8"
    }
  }
);
const DrawerNavigation = StackNavigator({
  DrawerStack: { screen: DrawerStack }
}, {
  headerMode: 'none',
  gesturesEnabled: false
})

// login stack
const LoginStack = StackNavigator({
  Login : { screen: Login },
  Registration: { screen: SignUp },
  ForgetPassword: { screen: ForgetPassword, navigationOptions: { title: 'Forgot Password' } },
  Logout:{screen: Logout}
}, {
  headerMode: 'float',
})
// Search Stack
const SearchStack = StackNavigator({
  Search : { screen: Question1 },
  Search1: { screen: Question2 },
  Search2: { screen: Question3 },
  Search3: { screen: Question4 },
  Search4: {screen: Question5},
  Result: {screen:Result},
  Logout:{screen:Logout}
}, {
  headerMode: 'none',
  transitionConfig: noTransitionConfig,
  gesturesEnabled: false


})
// Manifest of possible screens
const PrimaryNav = StackNavigator({
  Loading :{screen: Loading},
  loginStack: { screen: LoginStack },
  drawerStack: { screen: DrawerNavigation },
  searchStack: {screen : SearchStack},
  Logout:{screen:Logout}
}, {
  // Default config for all screens
  headerMode: 'none',
  title: 'Main',
  initialRouteName: 'Loading',
  transitionConfig: noTransitionConfig

})



