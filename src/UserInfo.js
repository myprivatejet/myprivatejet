import React from 'react'
import { StyleSheet, Platform, Image, Text, View,Button,TextInput,ScrollView } from 'react-native'
import firebase from 'react-native-firebase'
import { ListItem, Avatar, Overlay } from 'react-native-elements'
import {Header,Left,Right, Icon,Title,Body} from 'native-base'

export default class UserInfo extends React.Component {
  state = {firstname: "",
          lastname:"",
          email:"",
          phone:"",
          adress:"",
          city:"",
          state:"",
          zipcode:""
          }

  componentDidMount() {
    let user = firebase.auth().currentUser;
   firebase.database().ref('users/' + user.uid).once('value',(data) =>{
    let Data = data.toJSON();
    console.log(Data);
    this.setState({
     email : Data.email,
     firstname: Data.firstName,
     lastname : Data.lastName,
     phone : Data.phone,
     adress: Data.adress,
     city: Data.city,
     state: Data.state,
     zipcode: Data.zipCode
    })
  }
  )}
  
  AccessGallery = () => {
  
    this.props.navigation.navigate('Gallery');
  
  }

  static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="person" style = {{fontSize:24, color: tintColor }}/>
    )
  }
render() {
  const list = [
  {
    name: 'Email',
    subtitle: this.state.email,
    icon: 'email'
  },
  {
    name: 'Phone',
    subtitle: this.state.phone,
    icon: 'phone'

  },
  
  {
    name: 'Adress',
    subtitle: this.state.adress,
    icon: 'home'

  },
  ];

return (
  
    <View style = {styles.container}>
      <View style = {styles.profile}>
        <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
            <Left>
              <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
            <Body>
            <Title style = {{color: 'white'}}>Profile</Title>
            </Body>
            <Right>
              <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
          </Right>
      </Header>
        
        <View style = {styles.avatxt}>
          <View style ={styles.ProfileImage}>
              <Avatar
              containerStyle={{marginTop: 15}}
              size ="xlarge"
               source={{
               uri:
              'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
              }}
            rounded
            showEditButton
            onEditPress={this.AccessGallery}
            />
          </View>
          <View style = {styles.name}>
            <Text style ={styles.nameText}>{this.state.firstname}  </Text>
            <Text style ={styles.nameText}>{this.state.lastname}</Text>
          </View>
          <View style = {styles.location}>
            <Text style ={styles.locationText}>{this.state.city},  </Text>
            <Text style ={styles.locationText}>{this.state.state} </Text>
            <Text style ={styles.locationText}>{this.state.zipcode}</Text>
          </View>
      </View>
      </View>
      <View style = {styles.info}>
      {
        list.map((l, i) => (
          <ListItem
           containerStyle = {{borderBottomWidth:1}}
            key={i}
            title={l.name}
            subtitle={l.subtitle}
            leftIcon={{ name: l.icon }}
            chevron

          />
        ))
      }
         
      </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  profile:{
    backgroundColor: "#4583B8",
    height: "45%",
    width:"100%",
    alignItems:'center'
  },
  avatxt:{
      alignItems:'center',
      },
  ProfileImage:{
    height: 30,
    width:"100%",
    alignItems:'center',
  },

  name:{
    flexDirection:"row",
    padding:10,
    marginTop:"40%"
  },
  nameText:{
    fontSize:15,
    fontWeight:'bold',
    color:"white" 
  },
  location:{
    flexDirection:"row",
  },
  locationText:{
    fontSize:14,
    color:"white" 
  },
  info:{
    height: "55%",
    width:"100%",
    backgroundColor:"white"
  
  }
})