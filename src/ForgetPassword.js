import React from 'react'
import { StyleSheet, Text, TextInput, View, Button,ImageBackground,TouchableOpacity,Image } from 'react-native'
import firebase from 'react-native-firebase'

export default class ForgetPassword extends React.Component {
  state = { email: ''}

  static navigationOptions = {
   title: 'Forgot Password',
   headerStyle: {
      backgroundColor: '#4583B8',
    },
    headerTitleStyle: {
      color:'white',
      fontSize:18
    },
   headerTintColor: 'white',
}


  forgotPassword = (email) => {
    firebase.auth().sendPasswordResetEmail(email)
      .then(function (user) {
        alert('Please check your email...')
      }).catch(function (e) {
        console.log(e)
      })
  }


  render() {
    return (
    <ImageBackground source={require('./assets/sky2.jpg')} style={{width: '100%', height: '100%'}}>
      <View style={styles.container}>
        <Image
          style={{width:140,height:"20%", marginBottom:10, resizeMode: 'contain'}}
          source={require('./assets/Logo.png')}
        />
        <View style = {styles.emailWraper}>
          {this.state.errorMessage &&
            <Text style={{ color: 'red' }}>
              {this.state.errorMessage}
          </Text>}
          <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
          placeholderTextColor = "#3361D5"
           />
          </View>
            <TouchableOpacity onPress = {this.forgotPassword(this.state.email)}>
                <View style = {styles.button}>
                    <Text style = {{color: 'white', fontSize:15,fontWeight: 'bold'}}>Reset Password</Text>
                </View>
            </TouchableOpacity>
      </View>
    </ImageBackground>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  emailWraper:{
    backgroundColor: "white",
    height: "35%",
    width:"90%",
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: 'center',
    opacity: 0.7
  },
 
  textInput: {
    height: "23%",
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 20,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    fontSize : 15
},

button:{
  backgroundColor: '#4583B8',
  alignItems: 'center', 
  justifyContent: 'center',
  borderRadius: 30,
  marginTop:70,
  width:250,
  height:50
  
}
  
})