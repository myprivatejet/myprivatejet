import React from 'react'
import { StyleSheet, Text, TextInput, View, Button, ImageBackground,TouchableOpacity,Image } from 'react-native'
import firebase from 'react-native-firebase'
import {Header,Left,Right, Icon, Title,Body} from 'native-base'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export default class Question1 extends React.Component {
 state = {
    frequecy: '',
    milage: '',
  }

handleNext = () => {
      let user = firebase.auth().currentUser;
   firebase.database().ref('users/' + user.uid+'/answers/Question2').set({
        milage: this.state.milage,
        frequecy: this.state.frequecy,
        
      })
    this.props.navigation.navigate('Search2');
  
  }
  
  static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="search" style = {{fontSize:24, color: tintColor }}/>
    )
  }

  
render() {
    return (
     <ImageBackground source={require('./assets/sky.jpg')} style={{width: '100%', height: '100%'}}>
     <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
            <Left>
              <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
            <Body>
            <Title style = {{color: 'white'}}>Search</Title>
            </Body>
            <Right>
              <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
          </Right>
      </Header> 
      <View style={styles.container}>
       <View style = {styles.signupWraper}>
         
  <Text style= {styles.header}>How many hours do you fly? (Monthly)</Text>
         <TextInput
          style={styles.textInput}
          placeholder="3"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={frequecy => this.setState({ frequecy })}
          value={this.state.frequecy}
          placeholderTextColor = "#95a5a6"
        />
     
        <Text style= {styles.text2}>Is there an expected Increase? (%)</Text>
          <TextInput
          placeholder="50"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={milage => this.setState({ milage })}
          value={this.state.milage}
          placeholderTextColor = "#95a5a6"
        />
  
         
        </View>
         <TouchableOpacity onPress = {this.handleNext} >
             <View style={styles.rightContainer}>
               <View style = {styles.NextButton}>
                <Image source={require('./assets/Next.png')} style={styles.nextImage} />
                    <Text style = {{color: '#3361D5'}}>Next</Text>
               </View>
              </View>
         </TouchableOpacity>
      </View>
    </ImageBackground>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  signupWraper:{
    backgroundColor: "white",
    height: "55%",
    width:"90%",
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
    opacity: 0.7,
    padding:20,
  },
  header:{
    color:"#3361D5",
    marginTop: 20,
    fontSize:16,
    },
    text2:{
    color:"#3361D5",
    marginTop: 100,
    fontSize:16,
    },
   textInput: {
    height: "7%",
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop:10,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    fontSize:16
},
NextButton:{
  justifyContent: 'center',
  alignItems: 'center',
  height : 30,
  },
nextImage:{
  height: 80,
  width:80
},
rightContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    height : 30,
    width: "90%",
    marginTop: 100
  },
})