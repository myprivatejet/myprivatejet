import React from 'react'
import { StyleSheet, Text, TextInput, View, Button,ImageBackground,TouchableOpacity,Image } from 'react-native'
import firebase from 'react-native-firebase'
import {Icon} from 'native-base'

export default class Login extends React.Component {
  state = { email: '', password: '', errorMessage: null }


  handleLogin = () => {
    const { email, password } = this.state
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() =>{
        if (firebase.auth().currentUser.emailVerified == true)
        {
          this.props.navigation.navigate('Home')
        }
        else
        {
        alert("Please verify your password");      
         }
      }
       /*this.props.navigation.navigate(firebase.auth().currentUser.emailVerified ? 'Main' :'Login')*/)
      .catch(error => this.setState({ errorMessage: error.message }))
  }
  static navigationOptions = {
   title: 'Login',
   headerStyle: {
      backgroundColor: '#4583B8',
    },
    headerTitleStyle: {
      color:'white',
      fontSize:18
    },
   drawerIcon : ({tintColor}) => (
      <Icon name ="exit" style = {{fontSize:24, color: tintColor }}/>
    )
  }
  render() {
    return (
    <ImageBackground source={require('./assets/sky2.jpg')} style={{width: '100%', height: '100%'}}>
      <View style={styles.container}>
         <Image
          style={{width:140,height:"20%", marginBottom:10, resizeMode: 'contain'}}
          source={require('./assets/Logo.png')}
        />
        <View style = {styles.loginWraper}>
          <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
          placeholderTextColor = "#3361D5"
           />
          <TextInput
          secureTextEntry
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
          placeholderTextColor = "#3361D5"
          />
          {this.state.errorMessage &&
            <Text style={{ color: 'red', padding:20 }}>
              {this.state.errorMessage}
          </Text>}
          </View>
          <View style = {styles.buttons}>
          <TouchableOpacity onPress = {this.handleLogin}>
                <View style = {styles.button}>
                    <Text style = {{color: 'white', fontSize:20,fontWeight: 'bold'}}>Login</Text>
                </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Registration')}>
           <Text style={{ fontSize: 20, padding:5, marginTop:10, color :"#2c3e50"}}>Don't have an account? Sign Up</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgetPassword')}>
           <Text style={{color:'#2c3e50', fontSize: 15, padding:5}}>Forgot Password ?</Text>
          </TouchableOpacity>
          </View>
      </View>
    </ImageBackground>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginWraper:{
    backgroundColor: "white",
    height: "35%",
    width:"90%",
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: 'center',
    opacity: 0.7
  },
  textInput: {
    height: "23%",
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 20,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    fontSize:16
},

button:{
  backgroundColor: '#4583B8',
  alignItems: 'center', 
  justifyContent: 'center',
  borderRadius: 30,
  marginTop:30,
  width:200,
  height:"40%"
},
buttons:{
  height:"30%",
  alignItems: 'center', 
}
  
})