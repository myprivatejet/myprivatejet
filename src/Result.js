import React from 'react'
import { StyleSheet, Platform, Image, Text, View,TextInput,ScrollView,ImageBackground} from 'react-native'
import firebase from 'react-native-firebase'
import { Avatar, Overlay } from 'react-native-elements'
import {Header,Left,Right, Icon,Title,Body,Separator,Container,Content,Button,Thumbnail} from 'native-base'
import { TextInputMask } from 'react-native-masked-text'
//Import Components
import ListItem from './components/ListItem';
import AircraftDetail from './components/AircraftDetail'
import ListOwner from './components/ListOwner'
import OwnershipDetail from './components/OwnershipDetail'
// Import images
import HelicopterImg from './assets/Helicopter.jpg';
import VerylightjetImg from './assets/VeryLightJets.jpg'
import TurpopropsImg from './assets/Turboprops.jpg'
import LightJetImg from './assets/LightJet.jpg'
import HeavyJetImg from './assets/HeavyJet.jpg'
import MediumJetImg from './assets/mediumJet.jpg'
import Diamond from './assets/Diamond.png'
import Platinum from './assets/Platinum.png'
import Gold from './assets/Gold.png'
import Silver from './assets/Silver.png'
import Bronze from './assets/Bronze.png'

export default class UserInfo extends React.Component {
  state = {AircraftName:{
            aircraftNameCathegory:"",
            aircraftNameFeatures:"",
            aircraftNameModel:"",
            aircraftNameNumPassenger:"",
            aircraftNameSize:"",
            aircraftNameRange:"",
            aircraftNameSpeed:"",
            image:""
            },
            OwnershipState:{
            OwnershipStatusState:"",
            OwnershipCommitmentState:"",
            OwnershipPurchaseState:"",
            OwnershipAdvantageState:"",
            OwnershipTimeState:"",
            image:""
            },
            aircraftCategory:[],
            aircraftFeatures:[],
            aircraftModel:[],
            aircraftImages:[],
            aircraftNumPassenger:[],
            aircraftSize:[],
            aircraftRange:[],
            aircraftSpeed:[],
            OwnershipStatus:[],
            OwnershipCommitment:[],
            OwnershipPurchase:[],
            OwnershipTime:[],
            OwnershipAdvantages:[],
            OwnershipImage:[],
            SelectedAircraft: null,
            SelectedOwnership: null
            }


  componentDidMount() {
    
    let user = firebase.auth().currentUser;
    firebase.database().ref('users/' + user.uid +'/answers').once('value',(data) =>{
    let Data = data.toJSON();
    const from = Data.Question1.from.description;
    const to = Data.Question1.to.description
    const url = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${from}&destinations=${to}&key=AIzaSyCE3c1htF6-AvArLcMROWqUwbR5PH_U-Sk`
    return fetch(url)
      .then((response) => response.json())
      .then((data) => {
       
        /*
        Grabing the distance from the google distance matrix API and parsing it into an Integer
        */
        const distanceString = data.rows[0].elements[0].distance.text;
        if (distanceString.includes(",") == true){
          var newDistanceString = distanceString.replace(",", "");
          var distance = parseInt(newDistanceString, 10);
        }
        else{
          var distance = parseInt(distanceString, 10);
        }
        const peopleNum = Data.Question3.peoplenum;
      

        firebase.database().ref('AirCrafts/').once('value', (snapshot) => {
              let Snapshot = snapshot.toJSON();
              console.log(Snapshot);
        /*
        Aircraft select Algorithm 
         */
       AircraftSubmitHandler = () =>{
        
          this.setState(prevState =>{
            return{
              aircraftCategory: prevState.aircraftCategory.concat(prevState.AircraftName.aircraftNameCathegory),
              aircraftFeatures: prevState.aircraftFeatures.concat(prevState.AircraftName.aircraftNameFeatures),
              aircraftModel: prevState.aircraftModel.concat(prevState.AircraftName.aircraftNameModel),
              aircraftNumPassenger: prevState.aircraftNumPassenger.concat(prevState.AircraftName.aircraftNameNumPassenger),
              aircraftSize: prevState.aircraftSize.concat(prevState.AircraftName.aircraftNameSize),
              aircraftRange: prevState.aircraftRange.concat(prevState.AircraftName.aircraftNameRange),
              aircraftSpeed: prevState.aircraftSpeed.concat(prevState.AircraftName.aircraftNameSpeed),
              aircraftImages: prevState.aircraftImages.concat(prevState.AircraftName.image)
            };
          });
        };
        
        if (distance <= 450 || (peopleNum === "0-1" || peopleNum === "2-5"))
        {   
              this.setState({
                         AircraftName:{
                         aircraftNameCathegory: Snapshot.HELICOPTERS.Cathegory,
                         aircraftNameFeatures:Snapshot.HELICOPTERS.Features,
                         aircraftNameModel:Snapshot.HELICOPTERS.Models,
                         aircraftNameNumPassenger:Snapshot.HELICOPTERS.NumPassenger,
                         aircraftNameSize:Snapshot.HELICOPTERS.Size,
                         aircraftNameRange:Snapshot.HELICOPTERS.Range,
                         aircraftNameSpeed:Snapshot.HELICOPTERS.Speed,
                         image: HelicopterImg
                           }});
             AircraftSubmitHandler();
            
        }
        if (distance <= 2050 || (peopleNum === "0-1" || peopleNum === "2-5" || peopleNum === "6-8")) 
        {
            this.setState({
                         AircraftName:{
                         aircraftNameCathegory: Snapshot.TURBOPROPS.Cathegory,
                         aircraftNameFeatures:Snapshot.TURBOPROPS.Features,
                         aircraftNameModel:Snapshot.TURBOPROPS.Models,
                         aircraftNameNumPassenger:Snapshot.TURBOPROPS.NumPassenger,
                         aircraftNameSize:Snapshot.TURBOPROPS.Size,
                         aircraftNameRange:Snapshot.TURBOPROPS.Range,
                         aircraftNameSpeed:Snapshot.TURBOPROPS.Speed,
                         image: TurpopropsImg
                           }});
                AircraftSubmitHandler();
          }
        if (distance <= 1450 || (peopleNum === "0-1" || peopleNum === "2-5")){
          this.setState({
                         AircraftName:{
                         aircraftNameCathegory: Snapshot.VERYLIGHTJETS.Cathegory,
                         aircraftNameFeatures:Snapshot.VERYLIGHTJETS.Features,
                         aircraftNameModel:Snapshot.VERYLIGHTJETS.Models,
                         aircraftNameNumPassenger:Snapshot.VERYLIGHTJETS.NumPassenger,
                         aircraftNameSize:Snapshot.VERYLIGHTJETS.Size,
                         aircraftNameRange:Snapshot.VERYLIGHTJETS.Range,
                         aircraftNameSpeed:Snapshot.VERYLIGHTJETS.Speed,
                         image: VerylightjetImg
                           }});
             AircraftSubmitHandler();
        }
        if ((distance >=1100 || distance <= 2300) && (peopleNum === "6-8")){
         this.setState({
                         AircraftName:{
                         aircraftNameCathegory: Snapshot.LIGHTJETS.Cathegory,
                         aircraftNameFeatures:Snapshot.LIGHTJETS.Features,
                         aircraftNameModel:Snapshot.LIGHTJETS.Models,
                         aircraftNameNumPassenger:Snapshot.LIGHTJETS.NumPassenger,
                         aircraftNameSize:Snapshot.LIGHTJETS.Size,
                         aircraftNameRange:Snapshot.LIGHTJETS.Range,
                         aircraftNameSpeed:Snapshot.LIGHTJETS.Speed,
                         image: LightJetImg
                           }});
             AircraftSubmitHandler();
        }
        if ((distance >=2301 || distance <= 3450) && (peopleNum === "6-8")){
         this.setState({
                         AircraftName:{
                         aircraftNameCathegory: Snapshot.MEDIUMJETS.Cathegory,
                         aircraftNameFeatures:Snapshot.MEDIUMJETS.Features,
                         aircraftNameModel:Snapshot.MEDIUMJETS.Models,
                         aircraftNameNumPassenger:Snapshot.MEDIUMJETS.NumPassenger,
                         aircraftNameSize:Snapshot.MEDIUMJETS.Size,
                         aircraftNameRange:Snapshot.MEDIUMJETS.Range,
                         aircraftNameSpeed:Snapshot.MEDIUMJETS.Speed,
                         image: MediumJetImg
                           }});
             AircraftSubmitHandler();
        }
        if (distance >=3450 || (peopleNum === "0-1" || peopleNum === "2-5" || peopleNum === "6-8" || peopleNum === "9" || peopleNum ==="10+"))
        {
         this.setState({
                         AircraftName:{
                         aircraftNameCathegory: Snapshot.HEAVYJETS.Cathegory,
                         aircraftNameFeatures:Snapshot.HEAVYJETS.Features,
                         aircraftNameModel:Snapshot.HEAVYJETS.Models,
                         aircraftNameNumPassenger:Snapshot.HEAVYJETS.NumPassenger,
                         aircraftNameSize:Snapshot.HEAVYJETS.Size,
                         aircraftNameRange:Snapshot.HEAVYJETS.Range,
                         aircraftNameSpeed:Snapshot.HEAVYJETS.Speed,
                         image: HeavyJetImg
                           }});
             AircraftSubmitHandler();
        }
        });


        /* 
        
        Ownership selection Algorithm

        */
        const frequency = Data.Question2.frequecy;
        const Percentage = Data.Question2.milage*(frequency/100);
        const timeYear = frequency * 12;
        const totalTime = +timeYear + +Percentage;
        const ownership = Data.Question3.ownership;
        
        firebase.database().ref('OwnerShip/').once('value', (snap) => {
              let Snap = snap.toJSON();
        
        OwnershipSubmitHandler =() =>{
        
          this.setState(prevState =>{
            return{
              OwnershipStatus: prevState.OwnershipStatus.concat(prevState.OwnershipState.OwnershipStatusState),
              OwnershipCommitment: prevState.OwnershipCommitment.concat(prevState.OwnershipState.OwnershipCommitmentState),
              OwnershipPurchase: prevState.OwnershipPurchase.concat(prevState.OwnershipState.OwnershipPurchaseState),
              OwnershipAdvantages: prevState.OwnershipAdvantages.concat(prevState.OwnershipState.OwnershipAdvantageState),
              OwnershipTime: prevState.OwnershipTime.concat(prevState.OwnershipState.OwnershipTimeState),
              OwnershipImage:prevState.OwnershipImage.concat(prevState.OwnershipState.image)
            };
          });
        };
        if (ownership == true || totalTime > 400 || Data.Question4.accessability == "365/24/7")
        { 
          this.setState({
          OwnershipState:{
            OwnershipStatusState: Snap.WholeAircraftOwnership.Name,
            OwnershipCommitmentState:Snap.WholeAircraftOwnership.Commitment,
            OwnershipPurchaseState:Snap.WholeAircraftOwnership.PurchaseInc,
            OwnershipAdvantageState: Snap.WholeAircraftOwnership.Advantages,
            OwnershipTimeState:Snap.WholeAircraftOwnership.TimeFly,
            image: Diamond
            }});

           OwnershipSubmitHandler();
        }
        if (totalTime < 25)
        {
          this.setState({
            OwnershipState:{
            OwnershipStatusState: Snap.OnDemandCharter.Name,
            OwnershipCommitmentState:Snap.OnDemandCharter.Commitment,
            OwnershipPurchaseState:Snap.OnDemandCharter.PurchaseInc,
            OwnershipAdvantageState: Snap.OnDemandCharter.Advantages,
            OwnershipTimeState:Snap.OnDemandCharter.TimeFly,
            image: Bronze
            }
          });
           OwnershipSubmitHandler();
        }
        if ((totalTime >= 25 && totalTime <= 100) || Data.Question4.accessability == "1 - 3 days notice")
        {
          this.setState({OwnershipState:{
            OwnershipStatusState: Snap.ClosedFleetJetCard.Name,
            OwnershipCommitmentState:Snap.ClosedFleetJetCard.Commitment,
            OwnershipPurchaseState:Snap.ClosedFleetJetCard.PurchaseInc,
            OwnershipAdvantageState: Snap.ClosedFleetJetCard.Advantages,
            OwnershipTimeState:Snap.ClosedFleetJetCard.TimeFly,
            image: Gold
            }});
           OwnershipSubmitHandler();
        }
       if ((totalTime >= 50 && totalTime <=400) || Data.Question4.accessability == "1 - 3 days notice" )
        {
          this.setState({OwnershipState:{
            OwnershipStatusState: Snap.FractionalOwnership.Name,
            OwnershipCommitmentState:Snap.FractionalOwnership.Commitment,
            OwnershipPurchaseState:Snap.FractionalOwnership.PurchaseInc,
            OwnershipAdvantageState: Snap.FractionalOwnership.Advantages,
            OwnershipTimeState:Snap.FractionalOwnership.TimeFly,
            image: Platinum
            } });
          OwnershipSubmitHandler();
        }
        if ((totalTime >= 25 && totalTime <= 75) || Data.Question4.accessability == "One week or more notice")
            {
            this.setState({OwnershipState:{
            OwnershipStatusState: Snap.JetCharterCards.Name,
            OwnershipCommitmentState:Snap.JetCharterCards.Commitment,
            OwnershipPurchaseState:Snap.JetCharterCards.PurchaseInc,
            OwnershipAdvantageState: Snap.JetCharterCards.Advantages,
            OwnershipTimeState:Snap.JetCharterCards.TimeFly,
            image: Silver
            }});
            OwnershipSubmitHandler();
            }
        
        });
       })
      .catch((error) =>{
        console.error(error);
      });
    });
    }
  
  static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="person" style = {{fontSize:24, color: tintColor }}/>
    )
  }
 
 onAircraftSelected = (key) => {
   this.setState({
     SelectedAircraft: key
   });
  }

  onOwnershipSelected = (key) => {
   this.setState({
     SelectedOwnership: key
   });
  }

  modalClosedHandler = () =>{
     this.setState({
     SelectedAircraft: null,
     SelectedOwnership: null
   });
  }
render() {

  console.log('All models:', this.state.aircraftModel);

  
  const AircraftOutput = this.state.aircraftCategory.map((aircraft,i) => (
    <ListItem key ={i} aircraftName = {aircraft} AircraftImage={this.state.aircraftImages[i]} onSelectedAircraft={()=> this.onAircraftSelected(i)}/>
  ));
  const OwnershipOutput = this.state.OwnershipStatus.map((ownership,i) => (
    <ListOwner key ={i} OwnershipName = {ownership} OwnershipImage={this.state.OwnershipImage[i]} onSelectedOwnership={()=> this.onOwnershipSelected(i)}/>
  ));
  
return (
  
      <Container>
        <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
            <Left>
              <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
            <Body>
            <Title style = {{color: 'white'}}>Profile</Title>
            </Body>
            <Right>
              <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
          </Right>
        </Header>
        <ImageBackground source={require('./assets/sky2.jpg')} style={{width: '100%', height: '100%'}}>
           <ScrollView>
          <Text style={styles.title}>Aircrafts</Text>
          <AircraftDetail 
            SelectedAircraft={this.state.SelectedAircraft} 
            aircraftName = {this.state.aircraftCategory[this.state.SelectedAircraft]}  
            AircraftImage={this.state.aircraftImages[this.state.SelectedAircraft] } 
            aircraftFeatures ={this.state.aircraftFeatures[this.state.SelectedAircraft]} 
            aircraftPassenger={this.state.aircraftNumPassenger[this.state.SelectedAircraft]} 
            aircraftSize={this.state.aircraftSize[this.state.SelectedAircraft]} 
            aircraftRange={this.state.aircraftRange[this.state.SelectedAircraft]} 
            aircraftSpeed={this.state.aircraftSpeed[this.state.SelectedAircraft]} 
            AircraftNameModel = {this.state.aircraftModel[this.state.SelectedAircraft]} 
            onModalClosed={this.modalClosedHandler}/>
          <View>
          {AircraftOutput}
          </View>
          <Text style={styles.title}>Ownership Options</Text>
          <OwnershipDetail  
            SelectedOwnership={this.state.SelectedOwnership} 
            ownershipName = {this.state.OwnershipStatus[this.state.SelectedOwnership]} 
            OwnershipImage= {this.state.OwnershipImage[this.state.SelectedOwnership]}
            Commitment = {this.state.OwnershipCommitment[this.state.SelectedOwnership]} 
            FlyingTime={this.state.OwnershipTime[this.state.SelectedOwnership]} 
            PurInc={this.state.OwnershipPurchase[this.state.SelectedOwnership]} 
            OwnershipAdvantages={this.state.OwnershipAdvantages[this.state.SelectedOwnership]}
            onModalClosed={this.modalClosedHandler}/>
          <View style ={styles.listContainer}>
          {OwnershipOutput}
          </View>
         </ScrollView>
        </ImageBackground>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
  listContainer : {
    width:"100%"
  },
  title:{
    fontSize:18,
    color:"white",
    padding:10
  }
})