import React from 'react'
import { StyleSheet, Text, TextInput, View, Button, ImageBackground,TouchableOpacity,Image } from 'react-native'
import firebase from 'react-native-firebase'
import {Header,Left,Right, Icon, Title,Body} from 'native-base'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export default class Question1 extends React.Component {
  state = {
    from: '',
    To: '',
  }

handleNext = () => {
      let user = firebase.auth().currentUser;
   firebase.database().ref('users/' + user.uid+'/answers/Question1').set({
        from: this.state.from,
        to: this.state.to,
        
      })
    this.props.navigation.navigate('Search1');
  
  }
  
  static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="search" style = {{fontSize:24, color: tintColor }}/>
    )
  }

render() {
    return (
     <ImageBackground source={require('./assets/sky.jpg')} style={{width: '100%', height: '100%'}}>
     <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
            <Left>
              <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
            <Body>
            <Title style = {{color: 'white'}}>Search</Title>
            </Body>
            <Right>
              <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
          </Right>
      </Header> 
      <View style={styles.container}>
       <View style = {styles.signupWraper}>
        <Text style= {styles.header}>Where do you usually travel From ?</Text>
         <GooglePlacesAutocomplete
            placeholder='Enter Location'
            minLength={2}
            autoFocus={false}
            returnKeyType={'default'}
            fetchDetails={true}
            onPress={from => this.setState({ from })}
            value={this.state.from}
            styles={{
            textInputContainer: {
            backgroundColor:'white',
            borderTopWidth: 0,
            },
          }}
          currentLocation={false}
      
          query={{
         // available options: https://developers.google.com/places/web-service/autocomplete
            key: 'AIzaSyCE3c1htF6-AvArLcMROWqUwbR5PH_U-Sk',
          language: 'en', // language of the results
          types: '(cities)' // default: 'geocode'
          }}
          />
         <Text style= {styles.header}>To ?</Text>
         <GooglePlacesAutocomplete
            placeholder='Enter Location'
            minLength={2}
            autoFocus={false}
            returnKeyType={'default'}
            fetchDetails={true}
            onPress={to => this.setState({ to })}
            value={this.state.to}
            styles={{
            textInputContainer: {
            backgroundColor:'white',
            borderTopWidth: 0,
            },
          }}
          currentLocation={false}
      
          query={{
         // available options: https://developers.google.com/places/web-service/autocomplete
            key: 'AIzaSyCE3c1htF6-AvArLcMROWqUwbR5PH_U-Sk',
          language: 'en', // language of the results
          types: '(cities)' // default: 'geocode'
          }}
          />
         
        </View>
         <TouchableOpacity onPress = {this.handleNext} >
             <View style={styles.rightContainer}>
               <View style = {styles.NextButton}>
                <Image source={require('./assets/Next.png')} style={styles.nextImage} />
                    <Text style = {{color: '#3361D5'}}>Next</Text>
               </View>
              </View>
         </TouchableOpacity>
      </View>
    </ImageBackground>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  signupWraper:{
    backgroundColor: "white",
    height: "55%",
    width:"90%",
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
    opacity: 0.7,
    padding:20
  },
  header:{
    color:"#3361D5",
    marginTop: 20,
    fontSize:16,
    },
   textInput: {
    height: "7%",
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop:10,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    fontSize:16
},
NextButton:{
  justifyContent: 'center',
  alignItems: 'center',
  height : 30,
  },
nextImage:{
  height: 80,
  width:80
},
rightContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    height : 30,
    width: "90%",
    marginTop: 100
  },
})