import React from 'react'
import { StyleSheet, Platform, Image, Text, View,Button,TextInput,SafeAreaView, ScrollView,Dimensions,ImageBackground,TouchableOpacity } from 'react-native'
import firebase from 'react-native-firebase';
import UserInfo from './UserInfo';
import Question1 from './Question1';
import { Avatar, PricingCard } from 'react-native-elements';
import {Header,Left,Right, Icon, Title,Body} from 'native-base'

export default class Main extends React.Component {
  state = { currentUser: null,
            currentPassword: "",
            newPassword: "" }

  componentDidMount() {
    const { currentUser } = firebase.auth();
    this.setState({ currentUser })
  }
 
  signOutUser = async () => {
      try {
          await firebase.auth().signOut();
          this.prop.navigation.navigate('Login');
      } catch (e) {
          console.log(e);
      }
  }

  static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="home" style = {{fontSize:24, color: tintColor }}/>
    )
  }
  
  render() {
      const { currentUser } = this.state;
      return (
      <ImageBackground source={require('./assets/sky2.jpg')} style={{width: '100%', height: '100%'}}>
        <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
              <Left>
                <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
              </Left>
              <Body>
              <Title style = {{color: 'white'}}>Home</Title>
              </Body>
              <Right>
                <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
              </Right>
            </Header>
        <View style = {styles.container}>
                <Image
                style={{height: "40%",resizeMode: 'contain'}}
                source={require('./assets/aircraft.png')}
                />
              <PricingCard
                wrapperStyle={{width:280}}
                color="#4f9deb"
                price="$0"
                title="Free"
                info={['Aircraft Search', 'All Core Features']}
                button={{ title: ' GET STARTED', icon: 'flight-takeoff'}}
                onButtonPress={() => this.props.navigation.navigate('Search')}
              />
        </View>
      </ImageBackground>
      )
  }


}


const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center', 
    justifyContent: 'center',
    height:"100%",
    width:"100%"
  },
});
