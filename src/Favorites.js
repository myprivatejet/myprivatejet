import React from 'react'
import { StyleSheet, Text, TextInput, View, Button,ImageBackground,TouchableOpacity } from 'react-native'
import firebase from 'react-native-firebase'
import {Header,Left,Right, Icon,Title,Body} from 'native-base'


export default class Favorites extends React.Component {
 
 static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="star-half" style = {{fontSize:24, color: tintColor }}/>
    )
  }
  render() {
    return (
    <ImageBackground source={require('./assets/sky2.jpg')} style={{width: '100%', height: '100%'}}>
      <View style={{flex:1 , alignItems:'center'}}>
          <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
            <Left>
              <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
            <Body>
            <Title style = {{color: 'white'}}>Favorites</Title>
            </Body>
            <Right>
              <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
          </Right>
      </Header>
        <Text style = {{fontSize: 40, fontWeight:'bold',color: "white", marginTop:400}}> Comming Soon</Text>
      </View>
    </ImageBackground>

    )
  }
}
const styles = StyleSheet.create({
  
})