import React from 'react'
import { StyleSheet, Platform, Image, Text, View,Button,TextInput,ImageBackground,TouchableOpacity } from 'react-native'
import firebase from 'react-native-firebase'
import {Header,Left,Right, Icon,Title,Body} from 'native-base'


export default class ResetPassword extends React.Component {
  state = { currentPassword:"",
            newPassword: ""}
  componentDidMount() {
    const { currentUser } = firebase.auth()
    this.setState({ currentUser })
}

reauthenticate = (currentPassword) => {
    var user = firebase.auth().currentUser;
    var cred = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);
    return user.reauthenticateWithCredential(cred);
  }

onChangePasswordPress = () => {
    this.reauthenticate(this.state.currentPassword).then(() => {
      var user = firebase.auth().currentUser;
      user.updatePassword(this.state.newPassword).then(() => {
        alert("Password was changed");
        this.props.navigation.navigate('Main')
      }).catch((error) => { alert(error.message); });
    }).catch((error) => { alert(error.message) });
  }

static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="settings" style = {{fontSize:24, color: tintColor }}/>
    )
  }
render() {
return (
  <ImageBackground source={require('./assets/sky2.jpg')} style={{width: '100%', height: '100%'}}>
    <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
            <Left>
              <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
            <Body>
            <Title style = {{color: 'white'}}>ResetPassword</Title>
            </Body>
            <Right>
              <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
          </Right>
      </Header>
    <View style={styles.container}>
      <View style = {styles.resetWraper}>
        <TextInput
          secureTextEntry
          placeholder="current password"
          autoCapitalize="none"
          style={styles.textInput}
          value={this.state.currentPassword}
          onChangeText={(text) => {this.setState({ currentPassword: text })}}
          placeholderTextColor = "#3361D5"
        />

        <TextInput
          secureTextEntry
          placeholder="new password"
          autoCapitalize="none"
          style={styles.textInput}
          value ={this.state.newPassword}
          onChangeText={(text) => {this.setState({ newPassword: text })}}
          placeholderTextColor = "#3361D5"
        />
      </View>
      <TouchableOpacity onPress = {this.onChangePasswordPress}>
                <View style = {styles.button}>
                    <Text style = {{color: 'white', fontSize:20,fontWeight: 'bold'}}>Confirm</Text>
                </View>
      </TouchableOpacity>
      </View>
    </ImageBackground>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
 
  resetWraper:{
    backgroundColor: "white",
    height: "35%",
    width:"90%",
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: 'center',
    opacity: 0.7
  },
  textInput: {
    height: "23%",
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 20,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    fontSize:16
},
  button:{
  backgroundColor: '#4583B8',
  alignItems: 'center', 
  justifyContent: 'center',
  borderRadius: 30,
  marginTop:30,
  width:200,
  height:"25%"
  
}
})