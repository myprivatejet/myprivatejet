import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
import firebase from 'react-native-firebase'
import {Icon} from 'native-base'


export default class Logout extends React.Component {

   componentDidMount() {
    firebase.auth().signOut();
    this.props.navigation.navigate('Login');
  }

static navigationOptions = {
    drawerIcon : ({tintColor}) => (
      <Icon name ="exit" style = {{fontSize:24, color: tintColor }}/>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})