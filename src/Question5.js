import React from 'react'
import { StyleSheet, Text, TextInput, View, Button, ImageBackground,TouchableOpacity,Image } from 'react-native'
import {CheckBox} from 'react-native-elements'
import firebase from 'react-native-firebase'
import {Header,Left,Right, Icon, Title,Body,Picker} from 'native-base'
import { TextInputMask } from 'react-native-masked-text'

export default class Question5 extends React.Component {
 constructor(props) {
    super(props);

    this.state = {
      selected: "key0",
      phone:"",
      availability:"",
    }
  }
  onValueChange(value: string) {
    this.setState({
      selected: value,
      availability: value
    });
  }
handleNext = () => {
      let user = firebase.auth().currentUser;
   firebase.database().ref('users/' + user.uid+'/answers/Question5').set({
        phone: this.state.phone,
        availability: this.state.availability,                
      })
    this.props.navigation.navigate('Result');
  
  }
  

render() {
    return (
     <ImageBackground source={require('./assets/sky.jpg')} style={{width: '100%', height: '100%'}}>
     <Header style = {{width: '100%', backgroundColor:'#4583B8'}}>
            <Left>
              <Icon style = {{color: 'white'}} name = "menu" onPress={() => this.props.navigation.openDrawer()}/>
            </Left>
            <Body>
            <Title style = {{color: 'white'}}>Search</Title>
            </Body>
            <Right>
              <Icon style = {{color: 'white'}} name='home' onPress={() => this.props.navigation.navigate('Home')} />
          </Right>
      </Header> 
      <View style={styles.container}>
       <View style = {styles.signupWraper}>
          <Text style= {styles.header}>Would you like us to contact you for more details and services?</Text>
          <TextInputMask
           type={'cel-phone'}
           style={styles.textInput}
           options={
          {
          maskType: 'brl',
          withDDD: true,
          dddMask: '(999) 999-9999'
          }
          }
          onChangeText={phone => this.setState({ phone })}
          value={this.state.phone}
          placeholder="Best contact number"
          placeholderTextColor = "#95a5a6"
          />
          <Text style= {styles.header}>b.	Best time for a quick chat:</Text>
         <Picker
              note
              mode="dropdown"
              style={{ width: 150 }}
              selectedValue={this.state.selected}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="Click to select" value="key0" />
              <Picker.Item label="Morning" value="Morning" />
              <Picker.Item label="Afternoon" value="Afternoon" />
              <Picker.Item label="Evening " value="Evening" />
              <Picker.Item label="Night " value="Night" />
          </Picker>
        </View>
        <TouchableOpacity onPress = {this.handleNext} >
             <View style={styles.rightContainer}>
               <View style = {styles.NextButton}>
                <Image source={require('./assets/Next.png')} style={styles.nextImage} />
                    <Text style = {{color: '#3361D5'}}>Next</Text>
               </View>
              </View>
         </TouchableOpacity>
      </View>
    </ImageBackground>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  signupWraper:{
     backgroundColor: "white",
    height: "60%",
    width:"90%",
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
    opacity: 0.7,
    padding:20
  },
  header:{
    color:"#3361D5",
    marginTop: 30,
    fontSize:16
    },
   textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    marginTop: 0,
    fontSize:16
},
NextButton:{
  justifyContent: 'center',
  alignItems: 'center',
  height : 30,
  },
nextImage:{
  height: 80,
  width:80
},
rightContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    height : 30,
    width: "90%",
    marginTop: 100
  },
})