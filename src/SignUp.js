import React from 'react'
import { StyleSheet, Text, TextInput, View, Button, ImageBackground,TouchableOpacity,Image,ScrollView } from 'react-native'
import firebase from 'react-native-firebase'
import { TextInputMask } from 'react-native-masked-text'


export default class SignUp extends React.Component {
 state = { email: '',
           firstName:'', 
           lastName:'',
           phone:'',
           password: '', 
           errorMessage: null 
           }
  
  static navigationOptions = {
   title: 'Sign Up',
   headerStyle: {
      backgroundColor: '#4583B8',
    },
    headerTitleStyle: {
      color:'white',
      fontSize:18
    },
    headerTintColor: 'white',
  }
  handleSignUp = () => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
         firebase.auth().currentUser.sendEmailVerification().then(function() {
         // Email sent.
        }, function(error) {
          aler(error);
        });
        firebase.database().ref('users/' + res.user.uid).set({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        phone: this.state.phone,
        email: this.state.email,
        adress: '',
        city:'',
        state:'',
        zipCode:''
      })
        this.props.navigation.navigate('Login');
      })
      .catch(error => this.setState({ errorMessage: error.message }))
  }
render() {
    return (
     <ImageBackground source={require('./assets/sky.jpg')} style={{width: '100%', height: '100%'}}>
      <View style={styles.container}>
      <Image
          style={{width:140,height:"17%", marginBottom:10,resizeMode: 'contain'}}
          source={require('./assets/Logo.png')}
        />
      <View style = {styles.signupWraper}>
         <TextInput 
          style={styles.textInput}
          placeholder="First name"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={firstName => this.setState({ firstName })}
          value={this.state.firstName}
          placeholderTextColor = "#3361D5"
        />
         <TextInput
          style={styles.textInput}
          placeholder="Last Name"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={lastName => this.setState({ lastName })}
          value={this.state.lastName}
          placeholderTextColor = "#3361D5"
        />
        <TextInputMask
           type={'cel-phone'}
           style={styles.textInput}
           options={
          {
          maskType: 'brl',
          withDDD: true,
          dddMask: '(999) 999-9999'
          }
          }
          onChangeText={phone => this.setState({ phone })}
          value={this.state.phone}
          placeholder="Phone number"
          placeholderTextColor = "#3361D5"
          />
        <TextInput
          style={styles.textInput}
          placeholder="Email"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
          placeholderTextColor = "#3361D5"
        />
        <TextInput
          style={styles.textInput}
          secureTextEntry
          placeholder="Password"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
          placeholderTextColor = "#3361D5"
        />
        {this.state.errorMessage &&
          <Text style={{ color: 'red' }}>
            {this.state.errorMessage}
          </Text>}
      </View>
        <View style = {styles.buttons}>
         <TouchableOpacity onPress = {this.handleSignUp}>
                <View style = {styles.button}>
                    <Text style = {{color: 'white', fontWeight:'bold',fontSize:15}}>Sign Up</Text>
                </View>
         </TouchableOpacity>
         <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
           <Text style={{color:'#2c3e50', fontSize: 15, padding:5}}>Already have an account? Login.</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  signupWraper:{
    backgroundColor: "white",
    height: "62%",
    width:"90%",
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: 'center',
    opacity:0.7
  },
  header:{
    color:"#3361D5",
    fontSize: 40,
    },
   textInput: {
    height: "8%",
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 30,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
},
button:{
  backgroundColor: '#4583B8',
  alignItems: 'center', 
  justifyContent: 'center',
  borderRadius: 30,
  marginTop:10,
  width:160,
  height:40
  
},
buttons:{
  height:"15%",
  alignItems: 'center', 
  marginTop:5
}
})