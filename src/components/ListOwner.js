import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text,Icon,Button } from 'native-base'


const listOwner = (props) => (
   /* <TouchableOpacity style={styles.listItem} onPress={props.onSelectedOwnership} >
        <Text>{props.OwnershipName}</Text>
    </TouchableOpacity> */
    <ListItem thumbnail style={styles.listItem}>
        <Left>
            <Thumbnail square source={props.OwnershipImage} />
        </Left>
        <Body>
            <Text>{props.OwnershipName}</Text>
        </Body>
        <Right>
            <Button transparent onPress={props.onSelectedOwnership}>
                <Text>View</Text>
            </Button>
        </Right>
    </ListItem>

);

const styles = StyleSheet.create({
    listItem:{
        width:"100%",
        padding:10,
        marginBottom:5,
        backgroundColor:"#eee",
        flexDirection:"row",
        alignItems:"center",
        opacity:0.7
    },
    AircraftImage:{
        marginRight: 8,
        width:50,
        height:50
    }
})

export default listOwner;