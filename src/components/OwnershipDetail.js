import React from 'react';
import {Modal,View, Image,Text, StyleSheet,ImageBackground,ScrollView} from 'react-native';
import ListAdvantages from './ListAdvantages';
import {Button} from 'react-native-elements';


const ownershipDetail = props => {
    
    let modalContent = null;

    if(props.SelectedOwnership || props.SelectedOwnership == 0){
    
         const OwnershipAdvantageOutput = props.OwnershipAdvantages.Advantage.map((ownership,i) => (
    <ListAdvantages key ={i} ownershipAdvantage = {ownership} />
    ));
    
         modalContent = (
                <View>
                    <Image source={props.OwnershipImage} style={styles.ownershipImage} />
                    <View style = {styles.detailContainer}>
                    <ScrollView>
                    <Text style={styles.OwnershipName}>{props.ownershipName}</Text>
                    <Text style={styles.title}> Advantages: </Text>
                    <View style ={styles.listContainer}>
                        {OwnershipAdvantageOutput}
                    </View>
                    <Text style={styles.title}>Commitment:</Text>
                    <Text>{props.Commitment}</Text>
                    <Text style={styles.title}>Flying Time:</Text>
                    <Text>{props.FlyingTime}</Text>
                    <Text style={styles.title}>Purchase Increment:</Text>
                    <Text>{props.PurInc}</Text>
                    </ScrollView>
                    </View>
                </View>
         );
     }
    return(
    <Modal visible={props.SelectedOwnership !== null} animationType="slide" >
    <ImageBackground 
        source={require('../assets/sky2.jpg')} 
        style={{width: '100%', height: '100%'}}
        >
        <View style={styles.modalContainer}>
            {modalContent}
            <View>
            <Button 
                title ="Close"  
                onPress={props.onModalClosed}
                />
            </View>
        </View>
     </ImageBackground>
    </Modal>
    );
};

const styles = StyleSheet.create({
    modalContainer:{
        margin:22,
        
    },
   ownershipImage:{
        width: "30%",
        height: 110,
        padding:20,
        marginTop:50,
        borderRadius: 4,
        marginLeft:"32%"
    },
    OwnershipName:{
        fontWeight:"bold",
        textAlign:"center",
        fontSize:28,
        padding: 20,
        color:"#4583B8"
    },
     listContainer : {
    width:"100%"
  },
   title:{
        padding:5,
        color:"#4583B8",
        fontSize:18
    },
    detailContainer:{
         backgroundColor:"white",
        opacity:0.7,
        borderColor:"white",
        borderWidth:2,
        borderRadius: 4,
        padding: 20,
        marginTop:20,
        height:"60%"
    }
})
export default ownershipDetail;