import React from 'react';
import {Modal,View, Image,Text, Button, StyleSheet} from 'react-native';

 const listModel = (props) => (
    <View style={styles.listItem}>
        <Text>{props.aircraftModel}</Text>
    </View>
);


const styles = StyleSheet.create({
    listItem:{
        width:"100%",
        padding:5,
        marginBottom:3,
        flexDirection:"row",
        alignItems:"center"
    },
    
})

export default listModel;