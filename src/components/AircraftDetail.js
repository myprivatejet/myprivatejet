import React from 'react';
import {Modal,View, Image,Text, StyleSheet,ImageBackground,ScrollView} from 'react-native';
import ListModel from './ListModel';
import {Button} from 'react-native-elements';



const aircraftDetail = props => {
    
    let modalContent = null;

    if(props.SelectedAircraft || props.SelectedAircraft == 0){
    
     const AircraftModelOutput = props.AircraftNameModel.Model.map((aircraft,i) => (
    <ListModel key ={i} aircraftModel = {aircraft} />
    ));
    
    
    
         modalContent = (
            
                <View>
                    <Image source={props.AircraftImage} style={styles.aircraftImage} />
                      <View style = {styles.detailContainer}>
                        <ScrollView>
                        <Text style={styles.aircraftName}>{props.aircraftName}</Text>
                        <Text style = {styles.title}> Models: </Text>
                        <View style ={styles.listContainer}>
                            {AircraftModelOutput}
                        </View>
                        <Text style = {styles.title}>Features:</Text>
                        <Text>{props.aircraftFeatures}</Text>
                        <Text style = {styles.title}>Number of passenger:</Text>
                        <Text>{props.aircraftPassenger}</Text>
                        <Text style = {styles.title}>Size:</Text>
                        <Text>{props.aircraftSize}</Text>
                        <Text style = {styles.title}>Range:</Text>
                        <Text>{props.aircraftRange}</Text>
                        <Text style = {styles.title}>Speed:</Text>
                        <Text>{props.aircraftSpeed}</Text>
                        </ScrollView>
                      </View>
                </View>
         );
     }
    return(
    <Modal visible={props.SelectedAircraft !== null} animationType="slide" >
     <ImageBackground source={require('../assets/sky2.jpg')} style={{width: '100%', height: '100%'}}>
        <View style={styles.modalContainer}>
            {modalContent}
            <Button 
                title ="Close" 
                color="red" 
                onPress={props.onModalClosed}
                />
        </View>
    </ImageBackground>
    </Modal>
    );
};

const styles = StyleSheet.create({
    modalContainer:{
        margin:22,
    },
    aircraftImage:{
        width: "100%",
        height: 200,
        padding:20,
        marginTop:50,
        borderColor:"white",
        borderWidth:2,
        borderRadius: 4
    },
    aircraftName:{
        fontWeight:"bold",
        textAlign:"center",
        fontSize:28,
        padding: 20,
        color:"#4583B8"
    },
    listContainer : {
        width:"100%"
    },
    title:{
        padding:5,
        color:"#4583B8",
        fontSize:18
    },
    detailContainer:{
        backgroundColor:"white",
        opacity:0.7,
        borderColor:"white",
        borderWidth:2,
        borderRadius: 4,
        padding: 20,
        marginTop:20,
        height:"50%"
     }
})
export default aircraftDetail;